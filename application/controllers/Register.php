<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Register_user');
	}

	public function index()
	{
		$this->load->view('pages/register');
	}

	public function create_user()
	{
		$full_name = $this->input->post('full_name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$retype_password = $this->input->post('retype_password');
		$data = array (
			'user_name' => $full_name,
			'email' => $email,
			'password' => $password 
		);
		if(!$this->Register_user->create_user($data)){
			$data['msg'] = 'Ocurrio un errror';
			$this->load->view('pages/register',$data);
		}
		$data['msg'] = 'Ingresado correctamente!';
		$this->load->view('pages/register',$data);

		
	}
}